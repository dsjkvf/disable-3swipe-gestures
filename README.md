disable-3swipe-gestures
=======================


## About

This is a hack to disable the three fingers swipe gestures [hardcoded](https://discourse.gnome.org/t/how-to-disable-edit-touchpad-gestures-in-gnome-40/6081/6) in Gnome shell. Based on [gnome-shell-hammer](https://github.com/icedman/gnome-shell-hammer) tweaks by [icedman](https://github.com/icedman/).
